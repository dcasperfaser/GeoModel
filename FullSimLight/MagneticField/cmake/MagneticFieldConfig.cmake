get_filename_component(SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)

include(${SELF_DIR}/MagneticField-MagFieldInterfaces.cmake)
include(${SELF_DIR}/MagneticField-MagFieldServices.cmake)

